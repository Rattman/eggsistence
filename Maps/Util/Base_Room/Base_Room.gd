extends Node2D

class_name Base_Room

export(Vector2) var camera_limits := Vector2(1024, 600)

onready var pickups := $Pickups
onready var props := $Props

var egg : Egg
signal egg_activated(egg)

func initial_position() -> Vector2:
	return Vector2.ZERO

func reset_props() -> void:
	for node in props.get_children():
		if node is Door or node is Box: node.reset()

func reset_pickups(node : Node2D) -> void:
	for child in node.get_children():

		if child is Pickup: child.reset()
		else: reset_pickups(child) 

func _on_egg_activated(new_egg : Egg):
	self.emit_signal("egg_activated", new_egg)
