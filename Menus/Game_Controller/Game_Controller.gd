extends Node2D

export(PackedScene) var initial_room : PackedScene

var room : Base_Room

#Respawn_Info
var respawn_egg : Egg

func _ready():
	assert(initial_room != null, "Initial Room Null!")

	var new_room = initial_room.instance()

	$Map.call_deferred("add_child", new_room)

	$Player/Player/Camera.limit_right = new_room.camera_limits.x
	$Player/Player/Camera.limit_bottom = new_room.camera_limits.y

	# warning-ignore:return_value_discarded
	new_room.connect("egg_activated", self, "_on_egg_activated")

	yield(get_tree(), "idle_frame")

	$Player/Player.global_position = new_room.initial_position()

	room = new_room

func _on_egg_activated(new_egg : Egg) -> void:
	respawn_egg = new_egg

func _on_Player_dead(body : Player):
	body.global_position = respawn_egg.global_position

	if body.has_item: body.drop_item()
	yield(get_tree(), "idle_frame")

	room.reset_pickups(room.pickups)
	room.reset_props()
	body.spawn()
