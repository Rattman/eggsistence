extends KinematicBody2D

class_name Player

signal dead(body)

onready var animation : AnimationNodeStateMachinePlayback = $AnimationTree.get("parameters/playback")

#Ages
enum ages {baby, child, adult, old}
var age : int = ages.baby

#Physics
const MAX_SPEED : float = 500.0
const ACCEL : float = 800.0
const FRICTION : float = 0.9

var speed : Vector2 = Vector2.ZERO
var spd_multiplier : float = 1.0

#Itens
var has_item : bool = false
var item : Pickup = null

func _ready():
	spawn()

func spawn():
	self.show()
	age = ages.baby
	_change_age()

func take_item(item_taken : Pickup):
	assert(not has_item, "Trying to take an item while carrying an item.")
	has_item = true
	item = item_taken

	item.position = Vector2(0, -32)

	item.get_parent().call_deferred("remove_child", item)
	self.call_deferred("add_child", item)
	yield(get_tree(),"idle_frame")

	item.be_taken()

func drop_item():
	assert(has_item, "Trying to drop an item without an item.")
	has_item = false
	item.be_released()
	yield(get_tree(), "idle_frame")
	item.global_position = self.global_position

func _physics_process(delta):
	var dir : Vector2 = Vector2.ZERO
	dir.x = Input.get_action_strength("player_right") - Input.get_action_strength("player_left")
	dir.y = Input.get_action_strength("player_down") - Input.get_action_strength("player_up")

	dir = dir.normalized()

	if sign(dir.x) != sign(speed.x): speed.x *= FRICTION
	if sign(dir.y) != sign(speed.y): speed.y *= FRICTION

	if dir != Vector2.ZERO: speed += dir*ACCEL*delta
	else: speed *= FRICTION

	if speed.length() > MAX_SPEED : speed = speed.normalized()*MAX_SPEED
	elif speed.length() <= 10: speed = Vector2.ZERO

	if speed.length() == 0:
		animation.travel("Still")
		$AnimatedSprite.stop()
	else:
		animation.travel("Walking")
		$AnimatedSprite.play()

	# warning-ignore:return_value_discarded
	self.move_and_slide(speed*spd_multiplier)

func _input(event):
	if event.is_action_pressed("player_action") and age == ages.adult:
		if animation.get_current_node() != "Punch":
			animation.travel("Punch")

			for body in $Punch_Area.get_overlapping_bodies():
				if body is Punchable: body.be_punched(self)

	elif event.is_action_pressed("player_pickup") and has_item:
		self.drop_item()

	elif event.is_action_pressed("speed_up"):
		age += 1
		_change_age()

func die() -> void:
	self.hide()
	emit_signal("dead", self)

func _change_age() -> void:

	if age >= ages.old+1:
		die()
		return

	match(age):

		ages.baby:
			spd_multiplier = 1.0
			$AnimatedSprite.animation = "baby"

		ages.child:
			spd_multiplier = 2.0
			$AnimatedSprite.animation = "child"

		ages.adult:
			spd_multiplier = 1.0
			$AnimatedSprite.animation = "adult"

		ages.old:
			spd_multiplier = 1.0
			$AnimatedSprite.animation = "old"
