extends Area2D

export(Vector2) var push_direction : Vector2 = Vector2.ZERO

func _ready():
	set_direction()

func _physics_process(_delta):
	for body in self.get_overlapping_bodies():
		if body is Player:
			body.move_and_slide(body.MAX_SPEED*push_direction)

		elif body is Box and not body.moving:
			body.slide(push_direction)

func activate():
	print("I WAS ACTIVATED!")
	self.push_direction *= -1
	set_direction()

func set_direction():
	if push_direction.x == 1:
		$AnimatedSprite.rotation = 0
	if push_direction.x == -1:
		$AnimatedSprite.rotation = -PI
	if push_direction.y == 1:
		$AnimatedSprite.rotation = PI/2
	if push_direction.y == -1:
		$AnimatedSprite.rotation = 3*PI/2
