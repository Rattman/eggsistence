extends Node2D

class_name Egg

signal egg_activated(egg)

func respawn(body : Player):
	body.global_position = self.global_position
	body.spawn()

func _on_Egg_entered(_body):
	emit_signal("egg_activated", self)
