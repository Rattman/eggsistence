extends StaticBody2D

export(bool) var locked : bool = true

func _ready():
	locked = not locked
	activate()

func activate():
	locked = not locked
	if locked:
		$Sprite.frame = 0
	else:
		$Sprite.frame = 1
	$bodyCollide.call_deferred("set", "disabled", not locked)

