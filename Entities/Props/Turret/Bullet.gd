extends Area2D

var dir : Vector2
var speed : float

func _physics_process(delta):
	self.position += dir*speed*delta

func _on_body_entered(body):
	if body is Player:
		body.die()

	elif body is Box and not body.moving:
		body.be_shoot(self)

	on_hit()

func on_hit():
	self.call_deferred("free")
