extends StaticBody2D

export(bool) var active : bool = true

enum facing {north, east, south, west}
export(facing) var dir : int = facing.north

export(float) var shot_speed : float = 500.0
export(float) var shoot_delay : float = 2.0

export(PackedScene) var Bullet : PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	self.rotation = dir*PI/2
	$Sprite.rotation = -dir*PI/2
	if dir == facing.north:
		$Sprite.frame = 3
	elif dir == facing.south:
		$Sprite.frame = 1
	elif dir == facing.east:
		$Sprite.frame = 0
	elif dir == facing.west:
		$Sprite.frame = 2
	$Shot_Timer.wait_time = shoot_delay
	if active: $Shot_Timer.start()

func _on_shot_timer_timeout():
	var bullet = Bullet.instance()
	
	bullet.position = Vector2(0, -16)
	bullet.speed = self.shot_speed
	bullet.dir = Vector2.UP

	add_child(bullet)
	$Shot_Timer.start()

func activate():
	self.active = not self.active
	if active: $Shot_Timer.start()
	else: $Shot_Timer.stop()
