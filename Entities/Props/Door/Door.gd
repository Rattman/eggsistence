extends StaticBody2D

class_name Door

export(bool) var resets : bool = true

var unlocked : bool = false

func reset():
	if resets:
		unlocked = false
		$Sprite.frame = 0
		$bodyCollide.call_deferred("set", "disabled", false)

func _on_Area2D_area_entered(area):
	if not unlocked and area.is_in_group("key"):
		area.unlock()
		unlocked = true
		$Sprite.frame = 1
		$bodyCollide.call_deferred("set", "disabled", true)
