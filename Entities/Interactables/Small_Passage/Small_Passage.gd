extends Area2D

class_name Small_Passage

export(NodePath) var exit : NodePath
onready var exit_node : Small_Passage = self.get_node(exit)

var Player_node : Player

func _on_body_entered(body):
	Player_node = body

func _on_body_exited(_body):
	Player_node = null

func _input(event):
	if Player_node != null and Player_node.age == Player_node.ages.baby and event.is_action_pressed("player_action"):
		self.player_enter()

func player_enter() -> void:
	print("I SUCC U")
	Player_node.global_position = exit_node.global_position
	exit_node.player_exit()

# warning-ignore:function_conflicts_variable
func player_exit() -> void:
	print("I SPIT U")
	
