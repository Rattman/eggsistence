extends Area2D

export(Array, NodePath) var activated_objects : Array
export(bool) var lever

var activated : bool = false
var Player_node : Player

func _ready():
	if lever: 
		get_node('body').color = Color("ff0000")
		lever_flip()
	else: 
		$Sprite.frame = 2
	for object in activated_objects:
		assert(get_node(object).is_in_group("activatable"), "Non activatable object in list: " + str(object))

func _on_body_entered(body):
	Player_node = body

func _on_body_exited(_body):
	Player_node = null

func _input(event):
	if (lever or not activated) and Player_node != null\
					and Player_node.age == Player_node.ages.old\
					and event.is_action_pressed("player_action"):
		activated = not activated
		if lever: lever_flip()
		else: $Sprite.frame = 3
		for object in activated_objects:
			get_node(object).activate()

func lever_flip():
	if activated:
		$Sprite.frame = 5
	else:
		$Sprite.frame = 4
