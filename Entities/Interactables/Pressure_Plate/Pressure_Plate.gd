extends Area2D

export(Array, NodePath) var activated_objects : Array

var activated : bool = false

func _ready():
	for object in activated_objects:
		assert(get_node(object).is_in_group("activatable"), "Non activatable object in list: " + str(object))

func _on_body_entered(body):
	if body is Box: self.pressed(true)

func _on_body_exited(body):
	if body is Box: self.pressed(false)

func pressed(value):
		activated = value
		if value:
			$Sprite.frame = 1
		else:
			$Sprite.frame = 0
		for object in activated_objects:
			get_node(object).activate()
