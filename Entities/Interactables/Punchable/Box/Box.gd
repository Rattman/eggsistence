extends Punchable

class_name Box

onready var original_pos : Vector2 = self.position

const TILE_SIZE : int = 64
const MOVE_TIME : float = 0.2

export(bool) var resets : bool = true
var moving : bool = false

func _attemp_movement(dir : Vector2) -> void:
	if moving: return

	moving = true
	var movement := dir*TILE_SIZE
	$Path_Checker.position = movement
	yield(get_tree().create_timer(0.02), "timeout")

	if $Path_Checker.get_overlapping_bodies().size() > 0:
		moving = false
		return
	
	$Tween.interpolate_property(self, "global_position", self.global_position, 
					self.global_position+movement, MOVE_TIME, Tween.TRANS_CUBIC)
	$Tween.start()

func reset():
	if resets:
		if moving:
			$Tween.remove_all()
			moving = false

		self.position = original_pos

func be_punched(body : Player) -> void:
	var dir : Vector2 = self.global_position - body.global_position
	var uniform_dir : Vector2 = Vector2.ZERO

	if abs(dir.x) > TILE_SIZE/2.0: uniform_dir.x = sign(dir.x)
	if abs(dir.y) > TILE_SIZE/2.0: uniform_dir.y = sign(dir.y)

	_attemp_movement(uniform_dir)

func slide(dir : Vector2) -> void:
	_attemp_movement(dir)

func be_shoot(bullet : Area2D):
	var dir : Vector2 = self.global_position - bullet.global_position
	var uniform_dir : Vector2 = Vector2.ZERO

	if abs(dir.x) > TILE_SIZE/4.0: uniform_dir.x = sign(dir.x)
	if abs(dir.y) > TILE_SIZE/4.0: uniform_dir.y = sign(dir.y)

	_attemp_movement(uniform_dir)

func _on_tween_all_completed():
	moving = false
