extends Area2D

class_name Pickup

export(bool) var resets : bool = true

var player = null
var on_ground : bool = true

onready var original_parent := self.get_parent()
onready var original_position := self.position

func reset() -> void:
	if resets: self.position = original_position

func be_taken() -> void:
	on_ground = false
	self.call_deferred("set", "monitoring", false)
	yield(get_tree(),"idle_frame")

func be_released() -> void:
	on_ground = true
	self.call_deferred("set", "monitoring", true)
	self.get_parent().call_deferred("remove_child", self)
	self.original_parent.call_deferred("add_child", self)
	yield(get_tree(),"idle_frame")

func _on_body_entered(body : Node2D):
	player = body

func _on_body_exited(_body):
	if on_ground:
		player = null

func _input(event):
	if event.is_action_pressed("player_pickup") and on_ground and player != null:
		player.take_item(self)
