extends Pickup

export(bool) var breaks_on_use : bool = false
var used : bool = false

func reset() -> void:
	.reset()
	if resets and used:
		self.show()
		self.call_deferred("set", "monitorable", true)
		self.call_deferred("set", "monitoring", true)
		yield(get_tree(),"idle_frame")

func unlock() -> void:
	used = true
	self.call_deferred("set", "monitorable", false)
	self.call_deferred("set", "monitoring", false)
	self.hide()
	yield(get_tree(),"idle_frame")
	if breaks_on_use: resets = false

func be_taken() -> void:
	self.call_deferred("set", "monitorable", false)
	.be_taken()

func be_released() -> void:
	self.call_deferred("set", "monitorable", true)
	.be_released()


